import * as React from "react";
import '../../assets/style.scss'
export interface IProps {
    value: boolean,
    onValueChange: any,
    config: any

}
export interface IState {
    nodeRef: any,
    val: any
}
export default class BearSwitch extends React.Component<IProps, IState>{
    constructor(props:any){
        super(props);
        this.state = {
            nodeRef: React.createRef(),
            val: props.value ? props.value : false
        }
    }
    componentDidMount(){
        let nodeRef = this.state.nodeRef;
        nodeRef.current.checked = this.state.val;
        this.setState({nodeRef: nodeRef});
    }
    onChange(e:any){
        this.props.onValueChange(e.currentTarget.checked);
    }
    render(){
        return (
            <div className={"bearSwitch"}>
                <div className="toggleWrapper">
                    <input type="checkbox" className="dn" id="dn" ref={this.state.nodeRef} onChange={(e)=>this.onChange(e)}/>
                    <label htmlFor="dn" className="toggle">
                        <span className="toggle__handler"></span>
                    </label>
                    <div className="bear-body">
                        <span className="eye left"></span>
                        <span className="eye right"></span>
                    </div>
                </div>
            </div>
        )
    }
}
