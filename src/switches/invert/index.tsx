import * as React from "react";
import '../../assets/style.scss'
export interface IProps {
    value: boolean,
    onValueChange: any,
    config: any

}
export interface IState {
    nodeRef: any,
    val: any
}
export default class InvertSwitch extends React.Component<IProps, IState>{
    constructor(props:any){
        super(props);
        this.state = {
            nodeRef: React.createRef(),
            val: props.value ? props.value : false
        }
    }
    componentDidMount(){
        let nodeRef = this.state.nodeRef;
        nodeRef.current.checked = this.state.val;
        this.setState({nodeRef: nodeRef});
    }
    onChange(e:any){
        this.props.onValueChange(e);
        this.setState({
            val: e
        })
    }
    render(){
        return (
            <div className={"invertSwitch"}>
                <input id="toggle-on" className="toggle toggle-left" name="toggle" type="radio" ref={this.state.nodeRef} onClick={(e)=>this.onChange(false)} />
                <label htmlFor="toggle-off" className="btn">{this.props.config.rightText}</label>
                <input id="toggle-off" className="toggle toggle-right" name="toggle" type="radio" ref={this.state.nodeRef} onClick={(e)=>this.onChange(true)} />
                <label htmlFor="toggle-on" className="btn">{this.props.config.leftText}</label>
             </div>
        )
    }
}
