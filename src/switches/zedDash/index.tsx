import * as React from "react";
import '../../assets/style.scss'
export interface IProps {
    value: boolean,
    onValueChange: any,
    config: any

}
export interface IState {
    nodeRef: any,
    val: any
}
export default class ZedDashSwitch extends React.Component<IProps, IState>{
    constructor(props:any){
        super(props);
        this.state = {
            nodeRef: React.createRef(),
            val: props.value ? props.value : false
        }
    }
    componentDidMount(){
        let nodeRef = this.state.nodeRef;
        nodeRef.current.checked = this.state.val;
        this.setState({nodeRef: nodeRef});
    }
    onChange(e:any){
        this.props.onValueChange(e.currentTarget.checked);
    }
    render(){
        return (
            <div className={"zedDashSwitch"}>
                <span className={"switcher switcher-1"}>
                   <input type="checkbox" id={"switcher-1"} ref={this.state.nodeRef} onChange={(e)=>this.onChange(e)}/>
                   <label htmlFor="switcher-1"></label>
                </span>
            </div>
        )
    }
}
