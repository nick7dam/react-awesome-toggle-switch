import * as React from "react";
import '../../assets/style.scss'
export interface IProps {
    value: boolean,
    onValueChange: any,
    config: any

}
export interface IState {
    nodeRef: any,
    val: any
}
export default class BulbSwitch extends React.Component<IProps, IState>{
    constructor(props:any){
        super(props);
        this.state = {
            nodeRef: React.createRef(),
            val: props.value ? props.value : false
        }
    }
    componentDidMount(){
        let nodeRef = this.state.nodeRef;
        nodeRef.current.checked = this.state.val;
        this.setState({nodeRef: nodeRef});
    }
    onChange(e:any){
        this.props.onValueChange(e.currentTarget.checked);
    }
    render(){
        return (
            <div className={"BulbSwitch"}>
                <div className={"switch"}>
                    <input type={"checkbox"} name="toggle" ref={this.state.nodeRef} onChange={(e)=>this.onChange(e)}/>
                        <label htmlFor="toggle">
                            <i className={"bulb"}>
                                <span className={"bulb-center"}></span>
                                <span className={"filament-1"}></span>
                                <span className={"filament-2"}></span>
                                <span className={"reflections"}>
                                <span></span>
                                <span className={"sparks"}>
                                    <i className={"spark1"}></i>
                                    <i className={"spark2"}></i>
                                    <i className={"spark3"}></i>
                                    <i className={"spark4"}></i>
                                </span></span>
                            </i>
                        </label>
                    </div>
             </div>
        )
    }
}
