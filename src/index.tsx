import * as React from 'react';
import DefaultSwitch from './switches/default';
import RadialSwitch from './switches/radial';
import SimpleSwitch from './switches/simple';
import OldSchoolSwitch from './switches/oldSchool';
import GenderSwitch from './switches/gender';
import BulbSwitch from './switches/bulb';
import LampSwitch from './switches/lamp';
import LockSwitch from './switches/lock';
import NeonSwitch from "./switches/neon";
import ZedDashSwitch from "./switches/zedDash";
import TotoroSwitch from "./switches/totoro";
import ElasticSwitch from "./switches/elastic";
import SmileySwitch from "./switches/smiley";
import RollingSwitch from "./switches/rolling";
import DayNightSwitch from "./switches/dayNight";
import InvertSwitch from "./switches/invert";
import FlatSwitch from "./switches/flat";
import Neon2Switch from "./switches/neon2";
import BearSwitch from "./switches/bear";
const components:any = {
    DefaultSwitch: DefaultSwitch,
    RadialSwitch: RadialSwitch,
    SimpleSwitch: SimpleSwitch,
    OldSchoolSwitch: OldSchoolSwitch,
    GenderSwitch: GenderSwitch,
    BulbSwitch: BulbSwitch,
    LampSwitch: LampSwitch,
    LockSwitch: LockSwitch,
    NeonSwitch: NeonSwitch,
    ZedDashSwitch: ZedDashSwitch,
    TotoroSwitch: TotoroSwitch,
    ElasticSwitch: ElasticSwitch,
    SmileySwitch: SmileySwitch,
    RollingSwitch: RollingSwitch,
    DayNightSwitch: DayNightSwitch,
    InvertSwitch: InvertSwitch,
    FlatSwitch: FlatSwitch,
    Neon2Switch: Neon2Switch,
    BearSwitch: BearSwitch
};
export interface IProps {
    value: boolean;
    onChange:any;
    config?: {
        toggleTheme: string,
        title: string,
        leftText: string,
        rightText: string,
    };
}
export interface IState {
    val: boolean;
    configuration?: {
        toggleTheme: string,
        title: string,
        leftText: string,
        rightText: string
    }
}
export default class AwesomeToggleSwitch extends React.Component<IProps, IState> {
  constructor(props:any){
    super(props);
    this.state = {
      val: props.value ? props.value : false,
      configuration: props.config ? props.config : {
          toggleTheme: 'RadialSwitch',
          title: 'Title',
          leftText:'On',
          rightText:'Off'
      }
    };
  }
  componentDidMount(){}
  valChanged(e:any){
    this.props.onChange(e);
    this.setState({
        val: e
    })
  }
  render(){
      let name = this.state.configuration ? this.state.configuration.toggleTheme : 'DefaultSwitch';
      let component = components[name];
    return(
      <div style={{textAlign: "center", width: "100%", color: "white"}}>
          <p>{this.state.val.toString()}</p>
          {
              React.createElement(component, {value: this.state.val, onValueChange: (e:any)=>this.valChanged(e), config: this.state.configuration})
          }
      </div>
    )
  }
}
