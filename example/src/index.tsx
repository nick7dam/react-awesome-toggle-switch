import * as React from 'react';
import * as ReactDOM from 'react-dom';
import AwesomeToggleSwitch from "react-awesome-toggle-switch";
export interface IProps {
    value: boolean;
    onChange:any;
}
export interface IState {
    val: boolean;
    config: {
          toggleTheme: string,
          title: string,
          leftText: string,
          rightText: string,
    };
}
export default class ReactAwesomeToggleSwitchExample extends React.Component<{}, IState> {
  constructor(props:any) {
      super(props);
      this.state = {
          val: true,
          config:{
            toggleTheme: 'DefaultSwitch',
            title: 'Title',
            leftText:'On',
            rightText:'Off',
          }
      }
  }
  valChanged(e:any){
      this.setState({
          val: e
      });
  }
  render() {
    return (
    <div style={{width: '100%'}}>
        <AwesomeToggleSwitch value={this.state.val} onChange={(e:any)=>this.valChanged(e)} config={this.state.config}></AwesomeToggleSwitch>
    </div>
    );
  }
}
ReactDOM.render(<ReactAwesomeToggleSwitchExample/>, document.getElementById('root'));
