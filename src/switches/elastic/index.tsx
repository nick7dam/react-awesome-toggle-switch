import * as React from "react";
import '../../assets/style.scss'
export interface IProps {
    value: boolean,
    onValueChange: any,
    config: any

}
export interface IState {
    nodeRef: any,
    val: any
}
export default class ElasticSwitch extends React.Component<IProps, IState>{
    constructor(props:any){
        super(props);
        this.state = {
            nodeRef: React.createRef(),
            val: props.value ? props.value : false
        }
    }
    componentDidMount(){
        let nodeRef = this.state.nodeRef;
        nodeRef.current.checked = this.state.val;
        this.setState({nodeRef: nodeRef});
    }
    onChange(e:any){
        this.props.onValueChange(e.currentTarget.checked);
    }
    render(){
        return (
            <div className={"elasticSwitch"}>
                <label className="label">
                    <div className="toggle">
                        <input className="toggle-state" type="checkbox" name="check" value="check" ref={this.state.nodeRef} onChange={(e)=>this.onChange(e)}/>
                        <div className="toggle-inner">
                            <div className="indicator"></div>
                        </div>
                        <div className="active-bg"></div>
                    </div>
                    <div className="label-text">{this.props.config.title}</div>
                </label>
            </div>
        )
    }
}
